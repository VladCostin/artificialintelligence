package Model;

import robocode.ScannedRobotEvent;

/**
 * data about one of my enemy <br>
 * extends the class Bot, common data about all bots <br>
 * data used for targeting
 * 
 * @author Vlad Herescu
 * 
 */
public class EnemyBot extends Bot {

	/**
	 * the name of the bot, to identify him
	 */

	String m_name;

	/**
	 * direction used in Guess Factor is he moving ahead or back
	 */
	public int m_direction;

	/**
	 * the absolute value position.x of the enemy
	 */
	public double m_xAbsolutePosition;

	/**
	 * the absolute value position.y of the enemy
	 */
	public double m_yAbsolutePosition;

	/**
	 * the bearing of the tank comparing to our heading
	 */
	public double m_absoluteBearing;

	public double m_distance;

	public DataPoint m_point;

	public EnemyBot(ScannedRobotEvent e, double robotX, double robotY) {
		m_name = e.getName();
		m_distance = e.getDistance();
		m_absoluteBearing = e.getBearingRadians() + e.getHeadingRadians();

		m_xAbsolutePosition = robotX + Math.sin(m_absoluteBearing)
				* e.getDistance();
		m_yAbsolutePosition = robotY + Math.cos(m_absoluteBearing)
				* e.getDistance();
	}

	/**
	 * @param _name
	 *            : the name of the bot
	 */
	public EnemyBot(String _name) {

		m_name = _name;
		m_direction = 1;
	}

	public EnemyBot() {
		m_direction = 1;
	}

	public String getM_name() {
		return m_name;
	}

	public void setM_name(String m_name) {
		this.m_name = m_name;
	}

	/**
	 * resets the data about the bot, for example his name <br>
	 * in case the main enemy has died
	 */
	public void reset() {

		m_name = null;

	}

	/**
	 * @return : true if it has no name : it means the main enemy has died or
	 *         hasn't been selected <br>
	 *         false if there is a main enemy to attack
	 */
	public boolean none() {
		if (m_name == null)
			return true;
		return false;
	}

	/**
	 * @param e
	 *            : the data about the robot detected with the radar
	 */
	public void update(ScannedRobotEvent e) {

	}

}
