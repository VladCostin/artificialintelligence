package Movement;

public class PotentialTarget extends Potential {
	public Side s;
	
	public PotentialTarget (Side s, double width, double height, double strength, double decrease) {
		this.s=s;
		switch (s) {
		case LEFT:
			this.posX=0;
			break;
		case RIGHT:
			this.posX=width;
			break;
		case TOP:
			this.posY=height;
			break;
		case BOTTOM:
			this.posY=0;
			break;
		}
		str=strength;
		dec=decrease;
	}
	
	public PotentialTarget (Side s, double posX, double posY, double strength, double decrease, double botX, double botY) {
		this.s=s;
		switch (s) {
		case LEFT:
		case RIGHT:
			this.posX=posX;
			this.posY=botY;
			break;
		case TOP:
		case BOTTOM:
			this.posX=botX;
			this.posY=posY;
			break;
		}
		str=strength;
		dec=decrease;
	}
	
	public void setBot (double botX, double botY) {
		switch (s) {
		case LEFT:
		case RIGHT:
			this.posY=botY;
			break;
		case TOP:
		case BOTTOM:
			this.posX=botX;
			break;
		}
		this.botX=botX;
		this.botY=botY;
		this.calcForce();
	}
}
