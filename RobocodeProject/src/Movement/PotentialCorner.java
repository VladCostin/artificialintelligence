package Movement;

public class PotentialCorner extends Potential {
	public Corner c;
	
	public PotentialCorner (Corner c, double width, double height, double strength, double decrease) {
		this.c=c;
		switch (c) {
		case TOP_LEFT:
			this.posX=0;
			this.posY=height;
			break;
		case TOP_RIGHT:
			this.posX=width;
			this.posY=height;
			break;
		case BOTTOM_LEFT:
			this.posX=0;
			this.posY=0;
			break;
		case BOTTOM_RIGHT:
			this.posX=width;
			this.posY=0;
			break;
		}
		str=strength;
		dec=decrease;
	}
	
	public PotentialCorner (Corner c, double width, double height, double strength, double decrease, double botX, double botY) {
		this.c=c;
		switch (c) {
		case TOP_LEFT:
			this.posX=0;
			this.posY=height;
			break;
		case TOP_RIGHT:
			this.posX=width;
			this.posY=height;
			break;
		case BOTTOM_LEFT:
			this.posX=0;
			this.posY=0;
			break;
		case BOTTOM_RIGHT:
			this.posX=width;
			this.posY=0;
			break;
		}
		str=strength;
		dec=decrease;
	}
	
	public void setBot (double botX, double botY) {
		this.botX=botX;
		this.botY=botY;
		this.calcForce();
	}
	
	protected double normalizeBearing (double angle) {
		switch (c) {
		case TOP_LEFT: return 3*Math.PI/4;
		case TOP_RIGHT: return -3*Math.PI/4;
		case BOTTOM_LEFT: return -Math.PI/4;
		case BOTTOM_RIGHT: return -Math.PI/4;
		}
		return 0;
	}
}
