package Movement;

public class Potential {
	public double posX;
	public double posY;
	protected double str;
	
	public double botX;
	public double botY;
	
	protected double force;
	protected double bearing;
	protected double dec;
	
	double fx;
	double fy;
	
	protected Potential() {}
	
	public Potential (double posX, double posY, double strength, double decrease) {
		this.posX=posX;
		this.posY=posY;
		str=strength;
		dec=decrease;
	}
	
	public Potential (double posX, double posY, double strength, double decrease, double botX, double botY) {
		this.posX=posX;
		this.posY=posY;
		str=strength;
		this.botX=botX;
		this.botY=botY;
		dec=decrease;
		this.calcForce();
	}
	
	public void setBot (double botX, double botY) {
		this.botX=botX;
		this.botY=botY;
		this.calcForce();
	}
	
	public void setStrength (double strength) {
		str=strength;
		this.calcForce();
	}
	
	public void setStrength (double strength, double decrease) {
		str=strength;
		dec=decrease;
		this.calcForce();
	}
	
	protected void calcForce () {
		double x=posX-botX;
		double y=posY-botY;
		if (str<0) { // signal that this is our target bot
			force=Math.pow(Math.sqrt(x*x+y*y), dec)/str;
		}
		else {
			force=str/Math.pow(Math.sqrt(x*x+y*y), dec);
		}
		bearing=normalizeBearing(Math.PI/2 - Math.atan2(botY-posY, botX-posX));
		fx=Math.cos(bearing)*force;
		fy=Math.sin(bearing)*force;
	}
	
	protected double normalizeBearing (double angle) {
		double newAngle=angle;
		if (angle > Math.PI) newAngle-=2*Math.PI;
		else if (angle < -Math.PI) newAngle+=2*Math.PI;
		return newAngle;
	}
}
