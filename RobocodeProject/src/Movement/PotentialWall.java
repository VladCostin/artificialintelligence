package Movement;

public class PotentialWall extends Potential {
	public Side s;
	
	public PotentialWall (Side s, double width, double height, double strength, double decrease) {
		this.s=s;
		switch (s) {
		case LEFT:
			this.posX=0;
			break;
		case RIGHT:
			this.posX=width;
			break;
		case TOP:
			this.posY=height;
			break;
		case BOTTOM:
			this.posY=0;
			break;
		}
		str=strength;
		dec=decrease;
	}
	
	public PotentialWall (Side s, double posX, double posY, double strength, double decrease, double botX, double botY) {
		this.s=s;
		switch (s) {
		case LEFT:
		case RIGHT:
			this.posX=posX;
			this.posY=botY;
			break;
		case TOP:
		case BOTTOM:
			this.posX=botX;
			this.posY=posY;
			break;
		}
		str=strength;
		dec=decrease;
	}
	
	public void setBot (double botX, double botY) {
		switch (s) {
		case LEFT:
		case RIGHT:
			this.posY=botY;
			break;
		case TOP:
		case BOTTOM:
			this.posX=botX;
			break;
		}
		this.botX=botX;
		this.botY=botY;
		this.calcForce();
	}
	
	protected double normalizeBearing (double angle) {
		switch (s) {
		case LEFT: return Math.PI/2;
		case RIGHT: return -Math.PI/2;
		case TOP: return 0;
		case BOTTOM: return Math.PI;
		}
		return 0;
	}
}
