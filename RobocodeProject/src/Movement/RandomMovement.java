package Movement;

import java.util.Random;

import Core.MyRoboCode;

public class RandomMovement {
	private final int MAX_MOVE_DISTANCE = 500;
	private final int BORDER_THICKNESS = 100;
	private MyRoboCode robot;
	private double time;

	public RandomMovement(MyRoboCode robot) {
		this.robot = robot;
		time = System.currentTimeMillis();
	}

	public void move() {
		if (System.currentTimeMillis() > time + 1000) {
			double robotX = robot.getX();
			double fieldWidth = robot.getBattleFieldWidth();
			double robotY = robot.getY();
			double fieldHeigth = robot.getBattleFieldHeight();
			double currentHeading = robot.getHeading();
			if (robotX > fieldWidth - BORDER_THICKNESS
					|| robotX - BORDER_THICKNESS < 0
					|| robotY > fieldHeigth - BORDER_THICKNESS
					|| robotY - BORDER_THICKNESS < 0) {
				double requiredHeading = Math.toDegrees(Math.atan(Math
						.abs(fieldWidth / 2 - robotY)));
				double heading = 360 - (requiredHeading - currentHeading);
				if (heading < 0) {
					robot.setTurnLeft(Math.abs(heading));
				} else {
					robot.setTurnRight(heading);
				}
				robot.setAhead(fieldHeigth / 4);
			} else {
				Random r = new Random();
				int randInt = r.nextInt(MAX_MOVE_DISTANCE);
				int degrees = r.nextInt(360);

				if (degrees % 2 == 0) {
					robot.setTurnLeft(degrees);
				} else {
					robot.setTurnRight(degrees);
				}

				if (randInt % 2 == 0) {
					robot.setAhead(randInt + 200);
				} else {
					robot.setBack(randInt + 200);
				}
			}

			time = System.currentTimeMillis();
		}
	}
}
