package Movement;
import java.util.ArrayList;

import Core.MyRoboCode;
import Model.EnemyBot;


public class Movement {
	public static final int STR_WALL = 8000;
	public static final int DEC_WALL = 1;
	public static final int STR_CENTER_MIN = 0;
	public static final int STR_CENTER_MAX = 200;
	public static final int DEC_CENTER = 2;
	public static final int NUM_WALLS = 4;
	public static final int DISTANCE = 50;
	public static final int STR_ENEMY = 1000;
	public static final int STR_TARGET = -1000;
	
	private MyRoboCode robot;

	private PotentialWall[] fieldWall=new PotentialWall[4];
	private Potential[] fieldPoints=new Potential[3];
	private PotentialCorner[] fieldCorners=new PotentialCorner[4];
	
	private double fx;
	private double fy;
	
	private double botX;
	private double botY;
	
	private String targetName;
	private ArrayList<String> enemyNames=new ArrayList<String>();
	
	public Movement(MyRoboCode r) {
		double width=r.getBattleFieldWidth();
		double height=r.getBattleFieldHeight();
		robot = r;
		
		fieldWall[0]=new PotentialWall(Side.LEFT, width, height, STR_WALL, DEC_WALL);
		fieldWall[1]=new PotentialWall(Side.RIGHT, width, height, STR_WALL, DEC_WALL);
		fieldWall[2]=new PotentialWall(Side.TOP, width, height, STR_WALL, DEC_WALL);
		fieldWall[3]=new PotentialWall(Side.BOTTOM, width, height, STR_WALL, DEC_WALL);
		
		fieldCorners[0]=new PotentialCorner(Corner.TOP_LEFT, width, height, STR_WALL, DEC_WALL);
		fieldCorners[1]=new PotentialCorner(Corner.TOP_RIGHT, width, height, STR_WALL, DEC_WALL);
		fieldCorners[2]=new PotentialCorner(Corner.BOTTOM_LEFT, width, height, STR_WALL, DEC_WALL);
		fieldCorners[3]=new PotentialCorner(Corner.BOTTOM_RIGHT, width, height, STR_WALL, DEC_WALL);
		
		fieldPoints[0]=new Potential(width/2, height/2, getRandomCenterStrength(), DEC_CENTER);
		//fieldPoints[1]=new Potential(width/5, height/4, getRandomCenterStrength(), DEC_CENTER);
		fieldPoints[1]=new Potential(width-width/5, height/4, getRandomCenterStrength(), DEC_CENTER);
		fieldPoints[2]=new Potential(width/3, height-height/5, getRandomCenterStrength(), DEC_CENTER);
	}

	public void move() {
		
		resetCurrentData();
		
		findTarget();
		
		addForceOfEnemies();
		addForceOfTarget();
		
		addForceOfWalls();
		addForceOfCorners();
		addForceOfCenter();
	
		//System.out.println("Total: fx="+fx+" - fy="+fy);
		makeMove(botX+fx, botY+fy);
	}
	
	private void makeMove(double x, double y) {
		double angle=normalizeBearing(robot.getHeadingRadians() -absBearing(botX, botY, x, y));
	    int r=1;
	    if (angle > Math.PI/2) {
	    	angle -= Math.PI;
	    	r = -1;
	    }
	    else if (angle < -Math.PI/2) {
	    	angle += Math.PI;
	    }
	    //System.out.println("Move Left: "+angle+" - Distance: "+DISTANCE*r);
	    robot.setTurnLeftRadians(angle);
	    robot.setAhead(DISTANCE*r);
	}
	
	private void resetCurrentData() {
		this.botX=robot.getX();
		this.botY=robot.getY();
		this.fx=0;
		this.fy=0;
		
		enemyNames.clear();
		enemyNames.addAll(robot.m_enemies.keySet());
	}
	
	private void findTarget() {
		double[] distances=new double[enemyNames.size()];
		int length=0;
		int closest=0;
		
		for (int i=0; i<distances.length; i++) {
			length=robot.m_enemies.get(enemyNames.get(i)).size();
			distances[i]=robot.m_enemies.get(enemyNames.get(i)).get(length-1).m_distance;
		}
		
		for (int i=1; i<distances.length; i++) {
			if (distances[i]<distances[closest]) {
				closest=i;
			}
		}
		
		targetName=enemyNames.get(closest);
		enemyNames.remove(closest);
	}
	
	private void addForceOfEnemies() {
		Potential p;
		EnemyBot bot;
		int length=0;
		
		for (int i=0; i<enemyNames.size(); i++) {
			System.out.println(enemyNames.get(i));
			length=robot.m_enemies.get(enemyNames.get(i)).size();
			bot=robot.m_enemies.get(enemyNames.get(i)).get(length-1);
			p=new Potential(bot.m_xAbsolutePosition, bot.m_yAbsolutePosition, STR_ENEMY, 2, botX, botY);
			printData("Enemy", p);
			fx+=p.fx;
			fy+=p.fy;
		}
		
	}
	
	private void addForceOfTarget() {
		int length=robot.m_enemies.get(targetName).size();
		EnemyBot bot=robot.m_enemies.get(targetName).get(length-1);
		Potential p=new Potential(bot.m_xAbsolutePosition, bot.m_yAbsolutePosition, STR_TARGET, 1, botX, botY);
		printData("Target", p);
		fx+=p.fx;
		fy+=p.fy;
	}
	
	private void addForceOfWalls() {
		for (int i=0; i<NUM_WALLS; ++i) {
			fieldWall[i].setBot(this.botX, this.botY);
			fx+=fieldWall[i].fx;
			fy+=fieldWall[i].fy;
		}
	}
	
	private void addForceOfCorners() {
		for (int i=0; i<NUM_WALLS; ++i) {
			fieldCorners[i].setBot(this.botX, this.botY);
			fx+=fieldCorners[i].fx;
			fy+=fieldCorners[i].fy;
		}
	}
	
	private void addForceOfCenter() {
		for (int i=0; i<fieldPoints.length; ++i) {
			fieldPoints[i].setStrength(getRandomCenterStrength());
			fieldPoints[i].setBot(this.botX, this.botY);
			fx+=fieldPoints[i].fx;
			fy+=fieldPoints[i].fy;
		}
	}
	
	private void printWall (Side s, String name, Potential p) {
		System.out.print(s+" ");
		printData(name, p);
	}
	
	private void printData (String name, Potential p) {
		System.out.println(name+": x="+p.posX+" - y="+p.posY+" - fx="+p.fx+" - fy="+p.fy);
	}
	
	private void addForceOfRandomPoints () {
		
	}
	
	private int getRandomCenterStrength() {
		return STR_CENTER_MIN + (int)(Math.random()*((STR_CENTER_MAX-STR_CENTER_MIN) + 1));
	}
	
	private double absBearing (double x1, double y1, double x2, double y2) {
	    double x = x2 - x1;
	    double y = y2 - y1;
	    
	    double h = Math.sqrt(x*x+y*y);
	    
	    if (x > 0 && y > 0) {
	      return Math.asin(x / h);
	    }
	    if (x > 0 && y < 0) {
	      return Math.PI - Math.asin(x / h);
	    }
	    if (x < 0 && y < 0) {
	      return Math.PI + Math.asin(-x / h);
	    }
	    if (x < 0 && y > 0) {
	      return 2.0 * Math.PI - Math.asin(-x / h);
	    }
	    return 0;
	}
	
	private double normalizeBearing (double angle) {
		double newAngle=angle;
		if (angle > Math.PI) newAngle-=2*Math.PI;
		else if (angle < -Math.PI) newAngle+=2*Math.PI;
		return newAngle;
	}
}
