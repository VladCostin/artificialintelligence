package Targeting;

import java.util.ArrayList;

import robocode.ScannedRobotEvent;
import robocode.util.Utils;
import Core.CoreData;
import Core.MyRoboCode;
import Model.DataPoint;
import Model.EnemyBot;
import Model.WaveBullet;


public class Targeting 
{
	/**
	 * the algorithm to detect the most similar situations
	 */
	KNN m_kNearestNeigh;
	/**
	 * instance used to call methods 
	 * that give information about the bot's status
	 */
	MyRoboCode m_robot;
	
	
	/**
	 * data about the enemy used in methods
	 */
	EnemyBot m_enemy;
	
	public Targeting(MyRoboCode _robot) 
	{
		m_kNearestNeigh = new KNN();
		m_robot = _robot;
	}
	
	
	public void takeDecision(ScannedRobotEvent e)
	{
		
		createDataEnemy( e);
		checkWavesHitOrNot();
		WaveBullet wave = waveCode(e);
		target(wave);
	}
	
	
	/**
	 * creates the structure about the enemy, to be used in further methods
	 */
	public void createDataEnemy(ScannedRobotEvent e) {
		m_enemy = new EnemyBot();
		m_enemy.m_absoluteBearing = m_robot.getHeadingRadians() + e.getBearingRadians();
		m_enemy.m_xAbsolutePosition = m_robot.getX() + Math.sin(m_enemy.m_absoluteBearing) * e.getDistance();
		m_enemy.m_yAbsolutePosition = m_robot.getY() + Math.cos(m_enemy.m_absoluteBearing) * e.getDistance();
		m_enemy.m_distance = e.getDistance();
		
		//System.out.println("valorile sunt" + m_enemy.m_distance + " " + m_enemy.m_xAbsolutePosition + " " + m_enemy.m_yAbsolutePosition);
	}


	/**
	 * checks if the waves created previously have hit the target
	 * 
	 * @param e : data about the robot detected
	 */
	public WaveBullet waveCode(ScannedRobotEvent e)
	{
		DataPoint currentSituation = new DataPoint
				(Math.sin(m_enemy.m_absoluteBearing) * e.getDistance(),
				 Math.cos(m_enemy.m_absoluteBearing) * e.getDistance(),
				 e.getVelocity(), 
				 Utils.normalAbsoluteAngle(e.getHeadingRadians() - m_enemy.m_absoluteBearing)
				 );
		
		m_enemy.m_point = currentSituation;
		
		
		double power = Math.min(400 / e.getDistance(), 3);
		// don't try to figure out the direction they're moving , meaning detect the sens, not the heading
		// if they're not moving, just use the direction we had before
		if (e.getVelocity() != 0)
		{
			if (Math.sin(e.getHeadingRadians()-m_enemy.m_absoluteBearing)*e.getVelocity() < 0)
				m_enemy.m_direction = -1;
			else
				m_enemy.m_direction = 1;
		}
		
		WaveBullet newWave = new WaveBullet(m_robot.getX(), m_robot.getY(), m_enemy.m_absoluteBearing, power,
                m_enemy.m_direction, m_robot.getTime());
		
		CoreData.m_waves.put(newWave, currentSituation);
		
		return newWave;
	}
	
	/**
	 * take each wave created
		<br>if the wave has over passed the enemy => it has not hit him
		<br>=> we can remove him
	 */
	public void checkWavesHitOrNot()
	{
		ArrayList<WaveBullet> deleteWaves = new ArrayList<WaveBullet>();
		
		// not sure if I have implemented correctly the removing of the waves that over passed the enemy
		for (WaveBullet wave : CoreData.m_waves.keySet())
		{
			Double guessFactor = wave.checkHit(m_enemy.m_xAbsolutePosition, m_enemy.m_yAbsolutePosition, m_robot.getTime());
		
			if(guessFactor != null)
			{
				CoreData.m_guessFactors.put(CoreData.m_waves.get(wave), guessFactor); 
 				//System.out.println("GuesFactor : " + guessFactor);
				deleteWaves.add(wave);
				//m_waves.remove(wave);
			}	
		}
	//	System.out.println("Numarul de valuri de sters : " + deleteWaves.size());
		for(int i = 0; i < deleteWaves.size(); i++)
			CoreData.m_waves.remove(deleteWaves.get(i));
	}
	
	public double getPowerBullet()
	{
		 return Math.min(400 / m_enemy.m_distance, 3);
	}
	
	
	/**
	* @param wave 
	 * @param _currentPoint : the current data about the enemy we are aiming at
	*/
	public void target(WaveBullet wave)
	{
		m_kNearestNeigh.getMostSimilarDataPoints(m_enemy.m_point);
		Double angle = m_kNearestNeigh.getAngle(m_enemy.m_point, m_enemy.m_direction, wave.maxEscapeAngle(), m_enemy.m_distance); 
		
		if(angle != null)
		{
			double gunAdjust = Utils.normalRelativeAngle(
	                m_enemy.m_absoluteBearing - m_robot.getGunHeadingRadians() + angle);
			
			 m_robot.setTurnGunRightRadians(gunAdjust);
			 if (m_robot.getGunHeat() == 0 && gunAdjust < Math.atan2(9, m_enemy.m_distance)) 
	         {
	         	   m_robot.fire(getPowerBullet());
	         }
		}
	}
	

	
	
	public void updateCoreAfterRound()
	{
		System.out.println("Size-ul este" + CoreData.m_guessFactors.size());
		
		ArrayList<DataPoint> deleteWaves = new ArrayList<DataPoint>();
		int i;
		if(m_kNearestNeigh.getM_MaxSize() < CoreData.m_guessFactors.size() )
		{
			i = 0;
			for(DataPoint p : CoreData.m_guessFactors.keySet())
			{
				i++;
				deleteWaves.add(p);
				if(i > 2000)
					break;
			}
			
			for( i = 0; i < deleteWaves.size(); i++)
				CoreData.m_guessFactors.remove(deleteWaves.get(i));
		}
	}

	/**
	 * @return the m_kNearestNeigh
	 */
	public KNN getM_kNearestNeigh() {
		return m_kNearestNeigh;
	}

	/**
	 * @param m_kNearestNeigh the m_kNearestNeigh to set
	 */
	public void setM_kNearestNeigh(KNN m_kNearestNeigh) {
		this.m_kNearestNeigh = m_kNearestNeigh;
	}
	
	

}
