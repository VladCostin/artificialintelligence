package Targeting;

import java.util.ArrayList;

import Core.CoreData;
import Model.DataPoint;

/**
 * implementing KNN
 * @author Vlad Herescu
 *
 */
public class KNN {
	
	int m_similarSituations;
	
	/**
	 * how many old data shall we use
	 */
	int m_MaxSize;
	
	/**
	 * initializing data to use the KNN
	 */
	public KNN()
	{
		m_similarSituations = 1;
		m_MaxSize = 5000;
	}
	
	
	/**
	 * @param _currentPoint : the current situation of the enemy
	 * @param _direction : the direction he is going
	 * @param _maxEscapeAngle : the maximum angle the BOT can escape
	 * @return : the angle to shoot
	 */
	public Double getAngle(DataPoint _currentPoint , int _direction , double _maxEscapeAngle, double _distance )
	{
		ArrayList<DataPoint> points = getMostSimilarDataPoints( _currentPoint);
		
		if(points == null)
			return null;
		
		
		
		double bestAngle = 0;
		double bestDensity = 0;
		double density;
		double firingAngleA, firingAngleB;
		double botWidthAngle = Math.abs(36/_distance);
		int i, j;
		double guessFactorA;
		
		/*
		for(DataPoint pointA : points)
		{
			double guessFactorA = CoreData.m_guessFactors.get(pointA);
			firingAngleA = _direction * guessFactorA * _maxEscapeAngle;
			System.out.println("firingAngle A : " + firingAngleA);
		}
		System.out.println("###########################");
		*/
		
		guessFactorA = CoreData.m_guessFactors.get(points.get(0));
		firingAngleA = _direction * guessFactorA * _maxEscapeAngle;
		bestAngle = firingAngleA;
		
		
		/*
		for(i = 0 ; i < points.size(); i++)
		{
			DataPoint pointA = points.get(i);
		
			density = 0;
			guessFactorA = CoreData.m_guessFactors.get(pointA);
			firingAngleA = _direction * guessFactorA * _maxEscapeAngle;
			
		
			for(j = 0; j< points.size(); j++)
			{
				DataPoint pointB = points.get(j);
				
				if(pointA.equals(pointB)== false)
				{	
					
					double guessFactorB = CoreData.m_guessFactors.get(pointB);
					firingAngleB = _direction * guessFactorB * _maxEscapeAngle;
					
					double ux = Math.abs((firingAngleA - firingAngleB) / botWidthAngle );
				//	System.out.println("uraaa " +  guessFactorA + " " + guessFactorB);
					if( ux <= 1)
					{
						density++;
					}
				}
			}
			
			
			
			if(density > bestDensity)
			{
				bestAngle = firingAngleA;
				bestDensity = density;	
			}
		}
		*/
		//System.out.println("Best angle is "  + bestAngle);
	
		
		return bestAngle; 
		
	}

	/**
	 * @param _currentPoint : the current situation of the enemy regarding our state
	 */
	public ArrayList<DataPoint> getMostSimilarDataPoints(DataPoint _currentPoint)
	{
		if(CoreData.m_guessFactors.keySet().size() < 10)
			return null;
		
		ArrayList<DataPoint> m_mostNearest = new ArrayList<DataPoint>();
		ArrayList<Integer> m_indexAlreadyAdded = new ArrayList<Integer>();
	//	Set<DataPoint> m_historyPoints = CoreData.m_guessFactors.keySet();
	//	String[] GPXFILES1 = myset.toArray(new String[myset.size()]);
		
		DataPoint[] m_historyPoints = CoreData.m_guessFactors.keySet().toArray( new DataPoint[CoreData.m_guessFactors.keySet().size()]  );
		
		Integer indexAdded, index;
		double minDistance = 100;
		int nrSimilarSituation = Math.min(m_similarSituations,m_historyPoints.length); //m_historyPoints.size());
	//	System.out.println("Size of the history :  " + CoreData.m_guessFactors.size() + " " + CoreData.m_waves.size() + " " + nrSimilarSituation);
		
		for(int i = 0; i < nrSimilarSituation; i++)
		{
			indexAdded = 0;
			minDistance = 100;
			index = 0;
			for(DataPoint point : m_historyPoints)
			{
				if(m_indexAlreadyAdded.contains(index))
					continue;
				
				double distance = calcualateDistance(_currentPoint, point);
				if(distance < minDistance)
				{
					minDistance = distance;
					indexAdded = index;
				}
				
				
				
				index++;
			}
		//	System.out.println("Distanta : " + minDistance + " " + indexAdded);
			m_indexAlreadyAdded.add(indexAdded);
			
			if(minDistance < 1)
			{
			//	System.out.println("Distanta : " + minDistance + " " + indexAdded);
				m_mostNearest.add(m_historyPoints[indexAdded]);
			}
		}
	//	if(minDistance > 1)
	//		return null;
		
		//System.out.println("Distanta : " + minDistance);
		
		if(m_mostNearest.size() == 0)
			return null;
		
		return m_mostNearest;
		
	}

	/**
	 * @param _currentPoint : the current point data of the game
	 * @param _point : a point from the enemy's history points
	 * @return
	 */
	public double calcualateDistance(DataPoint _currentPoint, DataPoint _point) {
		
		double a = _currentPoint.getM_coordX() - _point.getM_coordX();
		double b =  _currentPoint.getM_coordY()  - _point.getM_coordY();
		double c = _currentPoint.getM_speed() - _point.getM_speed();
		double d = _currentPoint.getM_heading()  - _point.getM_heading();

		
		return a * a + b * b + c * c + d * d;
	}

	/**
	 * @return the m_similarSituations
	 */
	public int getM_similarSituations() {
		return m_similarSituations;
	}

	/**
	 * @param m_similarSituations the m_similarSituations to set
	 */
	public void setM_similarSituations(int m_similarSituations) {
		this.m_similarSituations = m_similarSituations;
	}

	/**
	 * @return the m_MaxSize
	 */
	public int getM_MaxSize() {
		return m_MaxSize;
	}

	/**
	 * @param m_MaxSize the m_MaxSize to set
	 */
	public void setM_MaxSize(int m_MaxSize) {
		this.m_MaxSize = m_MaxSize;
	}
	
	
	
}