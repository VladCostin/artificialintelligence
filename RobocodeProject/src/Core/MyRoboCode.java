package Core;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import Model.DataPoint;
import Model.EnemyBot;
import Model.MyBot;
import robocode.AdvancedRobot;
import robocode.BulletHitEvent;
import robocode.GunTurnCompleteCondition;
import robocode.RobotDeathEvent;
import robocode.RoundEndedEvent;
import robocode.Rules;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;
import Model.WaveBullet;
import Movement.Movement;
import Targeting.Targeting;
import Movement.Movement;

/**
 * main class that deals with events, calls decision algorithms, etc
 * 
 * @author Vlad Herescu
 * 
 */
public class MyRoboCode extends AdvancedRobot {
	private final int BOT_MOVEMENT_QUEUE_LENGTH = 100;
	private final int SCAN_ALL = 30;

	private int scanCounter = 0;
	/**
	 * the data about my bot : heading, speed, radar position
	 */
	MyBot m_bot;

	/**
	 * the data about my enemies
	 */
	public HashMap<String, List<EnemyBot>> m_enemies;

	public BulletHitEvent m_bulletHitEvent;

	/**
	 * the current enemy we are tracking
	 */
	EnemyBot m_currentEnemy;

	Double m_lastHeading;

	List<WaveBullet> waves = new ArrayList<WaveBullet>();

	/**
	 * contains the code which takes a decision regarding firing
	 */
	Targeting m_targetingDecision;
	Movement m_movement;

//	RandomMovement movement;

	/**
	 * contains public static values used in algorithms, etc
	 */

	CoreData m_core;

	public void run() {

		initData();
		initConstantsAboutGame();

		while (true) {
			turnRadarRight(Double.POSITIVE_INFINITY);
		}

	}

	/**
	 * initialize data about the BOTS and the state of my own BOT
	 */
	public void initData() {
		m_lastHeading = null;
		m_enemies = new HashMap<String, List<EnemyBot>>();
		m_currentEnemy = new EnemyBot();

//		movement = new RandomMovement(this);

		m_targetingDecision = new Targeting(this);
		m_movement = new Movement(this);

		if (CoreData.m_value == null)
			m_core = new CoreData();
		else
			System.out.println("NU ESTE NULL  null");

		setAdjustRadarForRobotTurn(true);
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);
	}

	/**
	 * determine the constants about the game <br>
	 * used to normalize the data gotten from scanning
	 */
	public void initConstantsAboutGame() {
		CoreData.m_Maxheading = (2 * Math.PI);
		CoreData.m_MaxWidth = getBattleFieldWidth();
		CoreData.m_MaxHeight = getBattleFieldHeight();
		CoreData.m_MaxSpeed = Rules.MAX_VELOCITY;

	}

	public void onScannedRobot(ScannedRobotEvent e) {

		// updateMainEnemy(e);

		// receiveRadarData(e);
		// detectStateFromHistory();
		// takeDecision(e);

		// updateMainEnemy(e);

		// this is used with guess factor, in case the
		// bot is moving randomly
		// waveCode(e);

		// m_targetingDecision.target(_currentPoint);

		checkNewBotAdded(e);
//		movement.move();

		m_targetingDecision.takeDecision(e);
		m_movement.move();
		robotRadarDecision(e);
		execute();
	}

	/**
	 * takes a decision for the radar, so it will have a lock on the robot
	 * 
	 * @param e
	 *            : data the robot detected
	 */
	public void robotRadarDecision(ScannedRobotEvent e) {
		scanCounter++;
		if (scanCounter > SCAN_ALL) {
			scanCounter = 0;
			for (int i = 0; i < 8; i++) {
				turnRadarRight(45);
			}
		} else {
			double radarTurn;

			radarTurn = getHeadingRadians() + e.getBearingRadians()
					- getRadarHeadingRadians();
			setTurnRadarRightRadians(2 * Utils.normalRelativeAngle(radarTurn));
		}
	}

	/*
	 * public void waveCode(ScannedRobotEvent e) {
	 * 
	 * 
	 * 
	 * // ... // (other onScannedRobot code, might be radar/movement) // ...
	 * 
	 * // Enemy absolute bearing, you can use your one if you already declare
	 * it. double absBearing = getHeadingRadians() + e.getBearingRadians();
	 * 
	 * // find our enemy's location: double ex = getX() + Math.sin(absBearing) *
	 * e.getDistance(); double ey = getY() + Math.cos(absBearing) *
	 * e.getDistance();
	 * 
	 * // Let's process the waves now: for (int i=0; i < waves.size(); i++) {
	 * WaveBullet currentWave = (WaveBullet)waves.get(i); // if the wave has
	 * over passed the enemy => it has not hit him // we can remove him if
	 * (currentWave.checkHit(ex, ey, getTime())) { waves.remove(currentWave);
	 * i--; } } double power = Math.min(400 / e.getDistance(), 3); // don't try
	 * to figure out the direction they're moving , meaning detect the sens, not
	 * the heading // if they're not moving, just use the direction we had
	 * before if (e.getVelocity() != 0) { if
	 * (Math.sin(e.getHeadingRadians()-absBearing)*e.getVelocity() < 0)
	 * direction = -1; else direction = 1; } // int[] currentStats =
	 * stats[(int)(e.getDistance() / 100)]; // It doesn't look silly now! int[]
	 * currentStats = stats; // This seems silly, but I'm using it to // show
	 * something else later WaveBullet newWave = new WaveBullet(getX(), getY(),
	 * absBearing, power, direction, getTime(), currentStats);
	 * 
	 * // detecting the segment where the enemy has been most of the time int
	 * bestindex = 15; // initialize it to be in the middle, guessfactor 0. for
	 * (int i=0; i<31; i++) if (currentStats[bestindex] < currentStats[i])
	 * bestindex = i;
	 * 
	 * // this should do the opposite of the math in the WaveBullet: double
	 * guessfactor = (double)(bestindex - (stats.length - 1) / 2) /
	 * ((stats.length - 1) / 2); double angleOffset = direction * guessfactor *
	 * newWave.maxEscapeAngle(); double gunAdjust = Utils.normalRelativeAngle(
	 * absBearing - getGunHeadingRadians() + angleOffset);
	 * setTurnGunRightRadians(gunAdjust);
	 * 
	 * // ??? if (getGunHeat() == 0 && gunAdjust < Math.atan2(9,
	 * e.getDistance())) { waves.add(newWave); fire(power); }
	 * 
	 * double radarTurn;
	 * 
	 * radarTurn = getHeadingRadians() + e.getBearingRadians() -
	 * getRadarHeadingRadians(); setTurnRadarRightRadians(2*
	 * Utils.normalRelativeAngle(radarTurn)); }
	 */

	/**
	 * receiving data about the state of the game
	 * 
	 * @param e
	 *            : the data about the robot detected
	 */
	public void receiveRadarData(ScannedRobotEvent e) {

	}

	private void detectStateFromHistory() {
		// TODO Auto-generated method stub

	}

	private void takeDecision(ScannedRobotEvent e) {
		double radarTurn, gunTurn, absoluteBearing;

		// double gunTurn = getHeadingRadians() + e.getBearingRadians() +
		// - getGunHeadingRadians();

		if (m_lastHeading != null) {
			// updateDataCircularMovement(e);
		}

		// actions taken after evaluationg the state
		m_lastHeading = e.getHeading();
		radarTurn = getHeadingRadians() + e.getBearingRadians()
				- getRadarHeadingRadians();
		setTurnRadarRightRadians(2 * Utils.normalRelativeAngle(radarTurn));

		// To avoid premature shooting, call the getGunTurnRemaining()
		// method to see how far away your gun is from the target and don't fire
		// until you're close.
		// setTurnGunRightRadians( Utils.normalRelativeAngle(gunTurn));
		// if (getGunHeat() == 0 && Math.abs(getGunTurnRemaining()) < 10)
		// setFire(1);

		/*
		 * double absoluteBearing = getHeadingRadians() + e.getBearingRadians();
		 * setTurnGunRightRadians(Utils.normalRelativeAngle(absoluteBearing -
		 * getGunHeadingRadians() + (e.getVelocity() *
		 * Math.sin(e.getHeadingRadians() - absoluteBearing) / 13.0)));
		 * setFire(2.0);
		 */
	}

	/**
	 * @param e
	 *            : the event about the enemy when he is using circular movement
	 */
	/*
	 * public void updateDataCircularMovement(ScannedRobotEvent e) {
	 * 
	 * double turnRate = e.getHeading() / m_lastHeading; double xRelative;
	 * double yRelative; double xT, yT; double absBearing =
	 * e.getBearingRadians() + getHeadingRadians();
	 * 
	 * xRelative = e.getDistance() * Math.sin(absBearing); yRelative =
	 * e.getDistance() * Math.cos(absBearing);
	 * 
	 * xT = xRelative - e.getVelocity()
	 * 
	 * 
	 * 
	 * }
	 */

	private void updateMainEnemy(ScannedRobotEvent e) {

		double xForce = 0, yForce = 0;

		if (m_currentEnemy.none())
			m_currentEnemy.setM_name(e.getName());

		if (e.getName().equals(m_currentEnemy.getM_name())) {

			// pretend that we rotate our bot and the enemy so our heading is 0
			// degrees
			// absolute bearing is the angle between our heading to this point,
			// rotateing to left
			double absBearing = e.getBearingRadians() + getHeadingRadians();
			double x = getX() + e.getDistance() * Math.sin(absBearing);
			double y = getY() + e.getDistance() * Math.cos(absBearing);

			// System.out.println("Punct: " + x + " " + y);
			// double distance=new Point2D.Double(x, y).distance(getX(),getY());

			// System.out.println(distance + " " + e.getDistance());

			xForce -= Math.sin(absBearing)
					/ (e.getDistance() * e.getDistance());
			yForce -= Math.cos(absBearing)
					/ (e.getDistance() * e.getDistance());

			double angle = Math.atan2(xForce, yForce);
			System.out.println("ungiul este" + angle);

			
			if (xForce == 0 && yForce == 0) {
				// If no force, do nothing
			} else if (Math.abs(angle - getHeadingRadians()) < Math.PI / 2) {
				setTurnRightRadians(Utils.normalRelativeAngle(angle
						- getHeadingRadians()));
				setAhead(Double.POSITIVE_INFINITY);
			} else {
				setTurnRightRadians(Utils.normalRelativeAngle(angle + Math.PI
						- getHeadingRadians()));
				setAhead(Double.NEGATIVE_INFINITY);
			}

		}

	}

	/**
	 * @param e
	 *            : event regarding an enemy scanned
	 */
	public void checkNewBotAdded(ScannedRobotEvent e) {
		EnemyBot bot = new EnemyBot(e, this.getX(), this.getY());

		// update bot
		if (m_enemies.containsKey(e.getName()) == false) {
			ArrayList<EnemyBot> l = new ArrayList<EnemyBot>(
					BOT_MOVEMENT_QUEUE_LENGTH);
			l.add(bot);
			m_enemies.put(e.getName(), l);
		} else {
			m_enemies.get(e.getName()).add(bot);
		}

	}

	public void onRobotDeath(RobotDeathEvent e) {
		m_enemies.remove(e.getName());
		if (e.getName().equals(m_currentEnemy.getM_name())) {
			m_currentEnemy.reset();
		}
	}

	public void onRoundEnded(RoundEndedEvent event) {
		m_targetingDecision.updateCoreAfterRound();
	}

	@Override
	public void onBulletHit(BulletHitEvent event) {
		super.onBulletHit(event);
		m_bulletHitEvent = event;
	}
}
